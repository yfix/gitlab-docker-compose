RAILS_ENV ?= development
RUN := run --rm

ifeq ($(USE_NFSMOUNT), true)
	DOCKER_COMPOSE_FILES := -f docker-compose.yml -f docker-compose.nfsmount.yml
else
	DOCKER_COMPOSE_FILES := -f docker-compose.yml
endif

DOCKER_COMPOSE := docker-compose $(DOCKER_COMPOSE_FILES)
DOCKER_COMPOSE_RUN := $(DOCKER_COMPOSE) $(RUN)

default: compose-test

test:
	bundle exec rspec --tag ~@mssql

compose-web:
	${DOCKER_COMPOSE_RUN} --service-ports web

compose-job:
	${DOCKER_COMPOSE_RUN} job

compose-db-prepare: compose-db-create compose-db-migrate

compose-db-create:
	${DOCKER_COMPOSE_RUN} -e "RAILS_ENV=${RAILS_ENV}" app bundle exec rails db:create

compose-db-drop:
	${DOCKER_COMPOSE_RUN} -e "RAILS_ENV=${RAILS_ENV}" app bundle exec rails db:drop

compose-db-migrate:
	${DOCKER_COMPOSE_RUN} -e "RAILS_ENV=${RAILS_ENV}" app bundle exec rails db:migrate

compose-db-rollback:
	${DOCKER_COMPOSE_RUN} -e "RAILS_ENV=${RAILS_ENV}" app bundle exec rails db:rollback

compose-psql:
	${DOCKER_COMPOSE_RUN} -e "RAILS_ENV=${RAILS_ENV}" app bundle exec rails db

compose-rails-console:
	${DOCKER_COMPOSE_RUN} -e "RAILS_ENV=${RAILS_ENV}" app bundle exec rails c

compose-bash:
	${DOCKER_COMPOSE_RUN} -e "RAILS_ENV=${RAILS_ENV}" app bash

psql:
	psql -U postgres -h pg gitlab

compose-down:
	docker-compose down

compose-stop:
	docker-compose stop

compose-rails-generate:
	${DOCKER_COMPOSE_RUN} app bundle exec rails g ${CMD}

compose-bundle:
	${DOCKER_COMPOSE_RUN} app bundle ${CMD}

compose-test:
	${DOCKER_COMPOSE_RUN} -e "RAILS_ENV=test" app bundle exec rspec --tag ~@mssql ${T}

compose-build:
	docker-compose build

compose-rebuild:
	docker-compose build --force-rm --no-cache
